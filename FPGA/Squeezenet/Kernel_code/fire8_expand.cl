#include<clc.h>
__kernel void __attribute__ ((reqd_work_group_size(27,1,1))) fire8_expand(__global float *fire8expand1x1_Weights_hw, __global float *fire8expand3x3_Weights_hw, __global float *fire8squeeze1x1_Features, __global float *fire8_Features, __global float *Layer8_pool_hw)
		{
	int x = get_local_id(0);
	int y = get_group_id(0);


	float Features = 0;

	for(int f=0; f<256; f++)
	{
		Features = 0;
		for(int n=0; n<64; n++)
		{
			float result = 0;
			result = fire8squeeze1x1_Features[n*27*27 + x*27 + y]*fire8expand1x1_Weights_hw[f*64+n];
			Features+= result;
		}
		//ReLU activation function computation
		if(Features<0)
			Features = 0;
		fire8_Features[f*27*27 + x*27+ y] = Features;
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	fire8_Features = fire8_Features + (27*27*256);
	for(int f=0; f<256; f++)
	{
		Features = 0;
		for(int n=0; n<64; n++)
		{
			float result = 0;
			for(int i = x-1; i<=x+1; i++)
			{
				for(int j=y-1; j<=y+1; j++)
				{
					int x_index = i-x+1;
					int y_index = j-y+1;
					int m = (y_index)+(x_index)*3;
					if(i<0 || j<0)
					{
						result+=0;
					}
					else if(j>26 || i>26)
					{
						result+=0;
					}
					else
					{
						result+= fire8squeeze1x1_Features[n*27*27 + i*27 + j]*fire8expand3x3_Weights_hw[m+f*9*64+n*9];
					}
				}
			}
			Features += result;
		}
		//ReLU activation function computation
		if(Features<0)
			Features = 0;
		fire8_Features[f*27*27 + x*27 + y] = Features;
	}

	barrier(CLK_LOCAL_MEM_FENCE);
	fire8_Features = fire8_Features - (27*27*256);

	float max = 0;
	{
		for(int output =0;output < 512 ;output++)
		{
			if(x%2 != 0)
			{
				if(y%2 != 0)
				{
					for(int i = x-1; i <= x+1; i++)
					{
						if(i>26) break;
						for(int j = y-1; j <= y+1; j++)
						{
							if(j>26) break;
							if(max < ((fire8_Features[output*27*27+i*27+j])))
								max =   ((fire8_Features[output*27*27+i*27+j])) ;

						}
					}
					Layer8_pool_hw[output*13*13+((x-1)/2)*13+(y-1)/2] = max;
					max = 0;
				}
			}
		}
	}

}
