#include<clc.h>
__kernel void __attribute__ ((reqd_work_group_size(13,1,1)))fire9(__global float *fire8_Features,__global float *fire9squeeze1x1_Weights_GPU, __global float *fire9squeeze1x1_Features,__global float *fire9expand1x1_Weights_GPU,__global float *fire9expand3x3_Weights_GPU,__global float *fire9_Features)
{
	float Features = 0;
	int x = get_local_id(0);
	int y = get_group_id(0);

	for(int f=0; f<64; f++)
	{
		Features = 0;
		for(int n=0; n<512; n++)
		{
               		Features+= fire8_Features[n*13*13 + x*13 + y]*fire9squeeze1x1_Weights_GPU[f*512+n];
		}
		//ReLU activation function computation
		if(Features<0)
			Features = 0;
		fire9squeeze1x1_Features[f*13*13 + x*13 + y] = Features;
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	for(int f=0; f<256; f++)
	{
		Features = 0;
		for(int n=0; n<64; n++)
		{
			float result = 0;
               		result = fire9squeeze1x1_Features[n*13*13 + x*13 + y]*fire9expand1x1_Weights_GPU[f*64+n];
			Features+= result;
		}
		//ReLU activation function computation
		if(Features<0)
			Features = 0;
		fire9_Features[f*13*13 + x*13+ y] = Features;
	}
	barrier(CLK_LOCAL_MEM_FENCE);
	fire9_Features=fire9_Features+(13*13*256);

	for(int f=0; f<256; f++)
	{
		Features = 0;
		for(int n=0; n<64; n++)
		{	float result = 0;
				for(int i = x-1; i<=x+1; i++)
				{
    					for(int j=y-1; j<=y+1; j++)
    					{
						int x_index = i-x+1;
						int y_index = j-y+1;
						int m = (y_index)+(x_index)*3;
         					if(i<0 || j<0)
						{
							result+=0;
						}
         					else if(j>12 || i>12)
						{
							result+=0;
						}
         					else
						{
               						result+= fire9squeeze1x1_Features[n*13*13 + i*13 + j]*fire9expand3x3_Weights_GPU[m+f*9*64+n*9];
						}
					}
				}
				Features += result;
		}
		//ReLU activation function computation
		if(Features<0)
			Features = 0;
		fire9_Features[f*13*13 + x*13 + y] = Features;
	}

}

